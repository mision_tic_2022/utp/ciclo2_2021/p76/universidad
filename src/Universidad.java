/**
 * Autor: Darío Andrés Peña Quintero
 * Fecha: 07 Julio 2021
 * Empresa: UTP - MINTIC
 * Descripción: Proyecto que me representa una composición entre una universidad
 * y una facultad
 */
public class Universidad{
    /************
     * Atributos
     ************/
    private String nombre;
    private String nit;
    private Facultad[] facultades;

    /***************
     * Constructor
     ***************/
    public Universidad(String nombre, String nit){
        this.nombre = nombre;
        this.nit = nit;
        //Se crea arreglo reserando un espacio en memoría de 5 posiciones para almacenar obejetos tipo Facultad
        this.facultades = new Facultad[5];
    }

    /**************
     * Consultores
    ***************/
    public Facultad getFacultad(int pos){
        return this.facultades[pos];
    }

    /**************
     * Métodos
     * (acciones)
     *************/
    public void crear_facultad(String nombre, int pos){
        //Crear objeto Facultad
        Facultad objFacultad = new Facultad(nombre, this);
        this.facultades[pos] = objFacultad;
        //Accede al objeto y obtiene el nombre de la facultad
        //String nombreF = objFacultad.getNombre();
        //System.out.println(nombreF);
    }


}

/**********************************************************
 * https://gitlab.com/mision_tic_2022/utp/ciclo2_2021/p76
 **********************************************************/