/**
 * Autor: Darío Andrés Peña Quintero
 * Fecha: 07 Julio 2021
 * Empresa: UTP - MINTIC
 * Descripción: Clase que me representa una facultad
 */
public class Facultad {
    /*************
     * Atributos
     ************/
    private String nombre;
    private Universidad universidad;

    /*************
     * Constructor
     *************/
    public Facultad(String nombre, Universidad universidad){
        this.nombre = nombre;
        this.universidad = universidad;
    }

    /*********** 
     * Consultores
     * (Getters)
    ************/
    public String getNombre(){
        return this.nombre;
    }

    /*****************
     * Modificadores
     * (Setters)
     ****************/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }


    /**********
     * Métodos
     * (Acciones)
     *********/
    public void crear_carreras(String nombre){
        System.out.println("Creando carrera..."+nombre);
    }
}
